Document: qt6-remoteobjects
Title: Debian qt6-remoteobjects Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-remoteobjects is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-remoteobjects/qt6-remoteobjects.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-remoteobjects/qt6-remoteobjects.ps.gz

Format: text
Files: /usr/share/doc/qt6-remoteobjects/qt6-remoteobjects.text.gz

Format: HTML
Index: /usr/share/doc/qt6-remoteobjects/html/index.html
Files: /usr/share/doc/qt6-remoteobjects/html/*.html
